import 'package:flutter/material.dart';
import 'package:banana/rules_page.dart';
import 'package:banana/about_page.dart';
import 'package:banana/studio_page.dart';
import 'package:banana/constants.dart';

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'ABOUT', icon: Icons.person),
  const Choice(title: 'THE STUDIO', icon: Icons.business),
  const Choice(title: 'GALLERY', icon: Icons.photo_camera),
  const Choice(title: 'FAQ', icon: Icons.list),
  const Choice(title: 'RULES', icon: Icons.report),
  const Choice(title: 'CONTACT', icon: Icons.directions),
];

class ChoiceCard extends StatelessWidget {
  const ChoiceCard({Key key, this.choice}) : super(key: key);

  final Choice choice;

  @override
  Widget build(BuildContext context) {
    return new LayoutBuilder(
      builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return SingleChildScrollView(
          padding: const EdgeInsets.all(10.0),
          child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _getWidget(context, choice)),
        );
      },
    );
  }

  List<Widget> _getWidget(BuildContext context, Choice choice) {
    List<Widget> widgets = new List();
    final TextStyle textStyle = Theme.of(context).textTheme.title;
    widgets.add(new Center(
        child: new Column(children: <Widget>[
          new Icon(choice.icon,
              size: 130.0, color: Constants.blueGrayWithAlpha),
          new Text(choice.title, style: textStyle),
          new Divider(
            indent: 10.0,color: Constants.blueGrayWithAlpha,
          ),
        ])));
    switch (choice.title) {
      case 'RULES':
        widgets.add(new RulesPage(title: 'Rules'));
        break;
      case 'ABOUT':
        //widgets
        //    .add(new Center(child: new Text(choice.title, style: textStyle)));
        widgets.add(new AboutPage(title: 'About'));
        //widgets.add(new PolygonWidget(Constants.aboutText, choice, textStyle));
        break;
      case 'THE STUDIO':
        widgets.add(new StudioPage(title: 'The Studio'));
        break;
      default:
        break;
    }
    return widgets;
  }
}

class PolygonWidget extends StatelessWidget {
  final String text;
  final Choice choice;
  final TextStyle textStyle;

  PolygonWidget(this.text, this.choice, this.textStyle);

  @override
  Widget build(BuildContext context) {
    return new Stack(children: <Widget>[
      //new Icon(choice.icon, size: 130.0, color: textStyle.color),
      new ClipPath(
          child: new Text(text), clipper: new PolygonClipper(130.0, 130.0))
    ]);
  }
}

class PolygonClipper extends CustomClipper<Path> {
  final double width;
  final double height;

  PolygonClipper(this.width, this.height);

  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.moveTo(width, 0.0);
    path.lineTo(size.height, 0.0);
    path.lineTo(size.width, size.height);
    path.lineTo(0.0, size.height);
    path.lineTo(0.0, height);
    path.lineTo(width, height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return false;
  }
}
