import 'package:flutter/material.dart';
import 'package:banana/constants.dart';

class RulesPage extends StatefulWidget {
  RulesPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _RulesPageState createState() => new _RulesPageState();
}

class _RulesPageState extends State<RulesPage> {
  @override
  Widget build(BuildContext context) {
    return new LayoutBuilder(
      builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return SingleChildScrollView(
          padding: const EdgeInsets.all(20.0),
          child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _getRuleAsRows()),
        );
      },
    );
  }

  List<Row> _getRuleAsRows() {
    int len = Constants.rules.length;
    List<Row> rows = new List();
    for (int i = 0; i < len; i++) {
      rows.add(new Row(
        children: <Widget>[
          new Flexible(
              child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Text(Constants.rules.elementAt(i).text,
                  style: Theme.of(context).textTheme.subhead)
            ],
          ))
        ],
      ));
    }
    return rows;
  }
}
