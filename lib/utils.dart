import 'package:http/http.dart' as http;
import 'package:image/image.dart';
import 'dart:io';
import 'package:html/parser.dart' show parse;
import 'package:html/dom.dart';
import 'package:banana/constants.dart';

class Utils {
  void download(String url, String path, String preview) {
    var data = http.readBytes(url);
    data.then((buffer) {
      File f = new File(path);
      RandomAccessFile randomAccessFile = f.openSync(mode: FileMode.write);
      randomAccessFile.writeFromSync(buffer);
      randomAccessFile.flushSync();
      randomAccessFile.closeSync();

      Image image = decodeJpg(new File(path).readAsBytesSync());

      Image thumbnail = copyResize(image, 200);
      new File(preview)..writeAsBytesSync(encodeJpg(thumbnail, quality: 90));
    });
  }

  void parseGallery() {
    http.read(Constants.faqUrl).then((contents) {
      var document = parse(contents);
      List<Element> figures =
          document.getElementsByClassName("gallery-item col col-auto");
      figures.forEach((Element e) => print(e));
    });
  }
}
