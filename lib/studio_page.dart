import 'package:flutter/material.dart';
import 'package:banana/constants.dart';

class StudioPage extends StatefulWidget {
  final String title;

  StudioPage({Key key, this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _StudioPageState();
  }
}

class _StudioPageState extends State<StudioPage> {
  @override
  Widget build(BuildContext context) {
    return new LayoutBuilder(
      builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return SingleChildScrollView(
          padding: const EdgeInsets.all(20.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Flexible(
                      child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(Constants.studioText,
                          style: Theme.of(context).textTheme.subhead),
                      new Divider(
                        indent: 10.0,
                      ),
                      new Image(
                          color: Colors.lightBlue,
                          colorBlendMode: BlendMode.softLight,
                          image: new AssetImage('images/studio/studio_1.jpg')),
                      new Divider(
                        indent: 10.0,
                        color: Constants.blueGrayWithAlpha,
                      ),
                      new Image(
                          color: Colors.lightBlue,
                          colorBlendMode: BlendMode.softLight,
                          image: new AssetImage('images/studio/studio_2.jpg'))
                      ,
                      new Divider(
                        indent: 10.0,
                        color: Constants.blueGrayWithAlpha,
                      ),
                      new Image(
                          color: Colors.lightBlue,
                          colorBlendMode: BlendMode.softLight,
                          image: new AssetImage(
                              'images/studio/studio_3.jpg'))
                      ,
                      new Divider(
                        indent: 10.0,
                        color: Constants.blueGrayWithAlpha,
                      ),
                      new Image(
                          color: Colors.lightBlue,
                          colorBlendMode: BlendMode.softLight,
                          image: new AssetImage(
                              'images/studio/studio_4.jpg'))
                      ,
                      new Divider(
                        indent: 10.0,
                        color: Constants.blueGrayWithAlpha,
                      ),
                      new Image(
                          color: Colors.lightBlue,
                          colorBlendMode: BlendMode.softLight,
                          image: new AssetImage(
                              'images/studio/studio_5.jpg'))
                    ],
                  )),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
