import 'package:flutter/material.dart';
import 'package:banana/constants.dart';

class AboutPage extends StatefulWidget {
  AboutPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _AboutPageState createState() => new _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  @override
  Widget build(BuildContext context) {
    return new LayoutBuilder(
      builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return SingleChildScrollView(
          //padding: const EdgeInsets.all(20.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Flexible(
                      child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(Constants.aboutText,
                          style: Theme.of(context).textTheme.subhead),
                      new Divider(
                        indent: 10.0,
                        color: Constants.blueGrayWithAlpha,
                      ),
                      new Image(
                          color: Colors.lightBlue,
                          colorBlendMode: BlendMode.softLight,
                          image: new AssetImage(
                              'images/about/augustin_abitei.jpg'))
                    ],
                  )),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
