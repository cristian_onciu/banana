import 'package:flutter/material.dart';
import 'package:html/parser.dart' show parse;
import 'package:html/dom.dart';

class Constants {
  static const String aboutUrl = "https://hipnotattoo.com/about/";
  static const String faqUrl = "https://hipnotattoo.com/faq/";
  static const String contactUrl = "https://hipnotattoo.com/contact/";
  static const String galleryUrl = "https://hipnotattoo.com/gallery/";

  static final Color blueGrayWithAlpha = Colors.blueGrey.withAlpha(200);

  static const String _rulesText = '''
1. Make sure you have eaten within 4 hours prior to your tattoo. I will not tattoo you on an empty stomach.

2. Do NOT consume any alcoholic beverages within the same day prior to getting your tattoo. I will not tattoo you if I smell alcohol on you. The same goes for narcotics.

3. Dress appropriately for your tattoo. I cannot tattoo through clothing, and many times it gets in the way if it’s too close to the tattoo.

4. Please be realistic on your expectations of a tattoo. I cannot do a quality tattoo on a client that moves excessively, is extremely sensitive to the touch, or is overly nervous.

5. You must be at least 18 years of age to receive a tattoo from me. This also means you have to behave like a grownup when getting tattooed by me. You will be allowed to have 1 person join you for the procedure to keep you company, and that is all. You don’t need an entourage, and the one companion doesn’t need to hold your hand. The better you behave yourself, the better the experience is going to be for you.

6. All cell phones must be turned off during the procedure, including anyone accompanying you.

7. No food is allowed in the tattoo area, but a contained drink with a cap is.

8. Do not bring your children in the shop, and do not leave them in the car. If you have children, you need to get a babysitter during your tattoo.

9. If you don’t know what you want tattooed, I will not set up an appointment with you until you do. It’s not my job to decide what you should get tattooed. If you can’t even do the preliminary thinking part about getting tattooed, you’re not ready to get a tattoo.

10. Think about what you’re getting tattooed. They are permanent, and therefore should be thought about thoroughly. Don’t get tattoos of images just because other people have done it. Do a little bit of thinking first.''';

  static List<Rule> rules = new List.generate(
          10,
          (int index) => new Rule(
              index, _rulesText.split(new RegExp(r"\d+\.")).elementAt(index)),
          growable: true)
      .where((Rule rule) => rule.text.isNotEmpty)
      .toList();

  static const String aboutText = '''
  Can you really get to know someone by reading their bio on their website? Probably not. It´s just a bunch of praise and accolades written by the author about themselves.

You´d probably get a better idea of who an artist is based on their body of work, their reputation and their style. In that case, refer back to the gallery portion of this website.

But in case you´re still wondering who Augustin is…well, I´m many things. Beyond being a tattooer and illustrator, I´m also a licensed psychotherapist, apprentice photographer, mountain hiker, amateur chef, tea addict, enthusiast of sarcasm, animal lover, husband and father…just to name a few.

But here is the official part. My name is Augustin Abitei. I was born in Romania and now I live in Estepona, province of Malaga, Spain. I did my first tattoo in 2010, and began tattooing full time 2011. Self taught, I strive for creative, dynamic, bold work. In general I like tattooing all kinds of styles, but I have an attraction for the black side of tattooing. Most of the time I work by appointment, so I like to have a conversation with the person willing to tattoo, to explain what my thoughts are about the design, to set together the size and place of the tattoo, then make a sketch, and then to execute the tattoo.

But to get a better idea of who Augustin is, just give me a call, a message or better…make an appointment for a tattoo. You´ll get to know me much better while spending a few hours getting tattooed. And it´ll be a lot more fun than reading this bio.
''';

  static const String studioText = '''
  In February of 2015 I managed, once again, to open my own studio, here in Estepona, under the same name, Hipno Tattoo, Piercing & Art, which I am extremely proud of. It’s a warm and intimate place, situated in the garden of Costa del Sol.

The studio fallows the most draconian rules of safety and sanitation, regarding location, personnel, equipment, tools and implements. We are 100% certified by the Junta de Andalucia´s Department of Health.

We provide for you:

- Award winning, quality tattooing in a safe environment.
- Experienced and knowledgeable artists.
- Privacy available at all times.
- All styles and designs available; traditional to contemporary.
- Excellent custom work.
- Sterilization and sanitary conditions that meet or exceed all regulations.
''';
}

class Rule {
  int ruleNumber;
  String text;

  Rule(this.ruleNumber, this.text);
}
